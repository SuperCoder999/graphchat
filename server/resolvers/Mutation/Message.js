const QueryError = require("../../errors/QueryError");

async function postMessage(parent, args, context) {
    if (!args.text) throw new QueryError("Text is empty");

    const data = await context.prisma.createMessage({ text: args.text });
    return data;
}

async function reactMessage(id, prisma, isLike) {
    const keyName = isLike ? "likeCount" : "dislikeCount";
    const message = await prisma.message({ id });

    if (!message) {
        throw new QueryError(`Message with id ${id} does not exist`);
    }

    const newLikeCount = message[keyName] + 1;

    const data = await prisma.updateMessage({
        data: { [keyName]: newLikeCount },
        where: { id }
    });

    return data;
}

async function likeMessage(parent, args, context) {
    const data = await reactMessage(args.id, context.prisma, true);
    return data;
}

async function dislikeMessage(parent, args, context) {
    const data = await reactMessage(args.id, context.prisma, false);
    return data;
}

module.exports = {
    postMessage,
    likeMessage,
    dislikeMessage
};
