const QueryError = require("../../errors/QueryError");

async function postComment(parent, args, context) {
    if (!args.text) throw new QueryError("Text is empty");

    const messageExists = await context.prisma.$exists.message({ id: args.messageId });

    if (!messageExists) {
        throw new QueryError(`Message with id ${args.messageId} does not exist`);
    }

    const data = await context.prisma.createComment({
        message: { connect: { id: args.messageId } },
        text: args.text
    });

    return data;
}

module.exports = {
    postComment
};
