async function comments(parent, args, context) {
    const data = await context.prisma.message({ id: parent.id }).comments();

    return {
        comments: data,
        count: data.length
    };
}

module.exports = {
    comments
};
