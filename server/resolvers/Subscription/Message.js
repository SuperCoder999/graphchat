async function newMessageSubscribe(parent, args, context) {
    const node = context.prisma.$subscribe.message({ mutation_in: ["CREATED"] }).node();
    return node;
}

async function updateMessageSubscribe(parent, args, context) {
    const node = context.prisma.$subscribe.message({ mutation_in: ["UPDATED"] }).node();
    return node;
}

const newMessage = {
    subscribe: newMessageSubscribe,
    resolve: payload => payload
};

const updateMessage = {
    subscribe: updateMessageSubscribe,
    resolve: payload => payload
};

module.exports = {
    newMessage,
    updateMessage
};
