const Message = require("./Mutation/Message");
const Comment = require("./Mutation/Comment");

module.exports = {
    ...Message,
    ...Comment
};
