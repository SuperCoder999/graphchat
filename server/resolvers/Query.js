const QueryError = require("../errors/QueryError");

async function messages(parent, args, context) {
    const orderQuery = args.orderBy ? { orderBy: args.orderBy } : {};
    const filterQuery = args.filterText ? { where: { text_contains: args.filterText } } : {};

    const limitQuery = String(args.skip) && String(args.first)
        ? { skip: args.skip, first: args.first }
        : {};
    
    const lastQuery = String(args.last)
        ? { last: args.last }
        : {};

    const data = await context.prisma.messages({
        ...orderQuery,
        ...filterQuery,
        ...(!lastQuery.last ? limitQuery : {}),
        ...lastQuery
    });

    const count = await context
        .prisma
        .messagesConnection(filterQuery)
        .aggregate()
        .count();

    return {
        messages: data,
        count
    };
}

async function comments(parent, args, context) {
    const messageExists = await context.prisma.$exists.message({ id: args.messageId });

    if (!messageExists) {
        throw new QueryError(`Message with id ${args.messageId} does not exist`);
    }

    const data = await context.prisma.comments({ where: { message: { id: args.messageId } } });

    return {
        comments: data,
        count: data.length
    };
}

module.exports = {
    messages,
    comments
};
