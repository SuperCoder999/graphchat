async function message(parent, args, context) {
    const data = context.prisma.comment({ id: parent.id }).message();
    return data;
}

module.exports = {
    message
};
