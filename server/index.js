const { GraphQLServer } = require("graphql-yoga");
const { prisma } = require("./prisma/prisma-client");
const Query = require("./resolvers/Query");
const Message = require("./resolvers/Message");
const Comment = require("./resolvers/Comment");
const Mutation = require("./resolvers/Mutation");
const Subscription = require("./resolvers/Subscription");

const resolvers = {
    Query,
    Message,
    Comment,
    Mutation,
    Subscription
};

const server = new GraphQLServer({
    resolvers,
    typeDefs: "./graphql/schema.graphql",
    context: { prisma }
});

server.start(() => {
    console.log("Server is listening on port 4000");
});
