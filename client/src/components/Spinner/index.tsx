import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

function Spinner(): JSX.Element {
    return (
        <Dimmer active inverted>
            <Loader size="massive" inverted />
        </Dimmer>
    );
}

export default Spinner;
