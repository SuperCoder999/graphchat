import React from "react";
import { Icon, List, Header, Feed, Segment } from "semantic-ui-react";
import { IMessage, IMessageList } from "../../containers/MessageList/types";
import moment from "moment";
import CommentList from "../../containers/CommentList";
import { Mutation } from "react-apollo";
import { DataProxy } from "apollo-cache";
import likeMessage from "../../queries/Mutation/Message/likeMessage";
import messages from "../../queries/Query/messages";
import dislikeMessage from "../../queries/Mutation/Message/dislikeMessage";

interface MessageProps {
    message: IMessage;
    queryVars: any;
}

function Message({ message, queryVars }: MessageProps): JSX.Element {
    function saveMessage(store: DataProxy, id: string, isLike: boolean) {
        const data: { messages: IMessageList } | null = store.readQuery({
            query: messages,
            variables: queryVars
        });

        const index = data!.messages.messages.findIndex((message: IMessage) => message.id === id);

        if (isLike) {
            data!.messages.messages[index].likeCount = message.likeCount + 1;
        } else {
            data!.messages.messages[index].dislikeCount = message.dislikeCount + 1;
        }

        store.writeQuery({
            query: messages,
            data
        });
    }

    return (
        <Feed.Event as={Segment} className="fluid" style={{ marginBottom: 30, padding: 10, marginTop: 10 }}>
            <Feed.Content textalign="left">
                <Feed.Summary>
                    <Feed.User>
                        <Icon name="barcode" className="inline-padded" />
                        {message.id.slice(-8, -1)}
                    </Feed.User>
                    <Feed.Date>
                        {moment(message.createdAt).calendar()}
                    </Feed.Date>
                </Feed.Summary>
                <Feed.Extra text>
                    {message.text}
                </Feed.Extra>
                <Feed.Meta>
                    <Mutation
                        mutation={likeMessage}
                        variables={{ id: message.id }}
                        update={(store: DataProxy, result: { data?: { likeMessage: { id: string } } }) => {
                            saveMessage(store, result.data!.likeMessage.id, true);
                        }}
                    >
                        {(likeMessage: () => void) =>
                            <List.Item as="a" onClick={likeMessage}>
                                <Icon name="thumbs up" className="toolbar-icon" />
                                {message.likeCount}
                            </List.Item>
                        }
                    </Mutation>
                    <Mutation
                        mutation={dislikeMessage}
                        variables={{ id: message.id }}
                        update={(store: DataProxy, result: { data?: { dislikeMessage: { id: string } } }) => {
                            saveMessage(store, result.data!.dislikeMessage.id, false);
                        }}
                    >
                        {(dislikeMessage: () => void) =>
                            <List.Item as="a" onClick={dislikeMessage}>
                                <Icon name="thumbs down" className="toolbar-icon" />
                                {message.dislikeCount}
                            </List.Item>
                        }
                    </Mutation>
                </Feed.Meta>
                <Header dividing as="h4" style={{ marginTop: 10 }}>Comments</Header>
                <CommentList queryVars={queryVars} comments={message.comments.comments} messageId={message.id} />
            </Feed.Content>
        </Feed.Event>
    );
}

export default Message;
