import React from "react";
import { Comment as CommentUI } from "semantic-ui-react";
import { IComment } from "../../containers/MessageList/types";
import moment from "moment";

interface CommentProps {
    comment: IComment;
}

function Comment({ comment }: CommentProps): JSX.Element {
    return (
        <CommentUI className="comment-padded">
            <CommentUI.Content>
                <CommentUI.Metadata>
                    {comment.id.slice(-8, -1)}
                    {" - "}
                    {moment(comment.createdAt).calendar()}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {comment.text}
                </CommentUI.Text>
            </CommentUI.Content>
        </CommentUI>
    );
}

export default Comment;
