import React, { useState } from "react";
import { Form, Button, TextAreaProps } from "semantic-ui-react";
import postComment from "../../queries/Mutation/Comment/postComment";
import { Mutation } from "react-apollo";
import { IComment, IMessageList, IMessage } from "../../containers/MessageList/types";
import { DataProxy } from "apollo-cache";
import messagesQuery from "../../queries/Query/messages";

interface CommentFormProps {
    messageId: string;
    queryVars: any;
    reload: () => void;
}

function CommentForm({ messageId, queryVars, reload }: CommentFormProps) {
    const [open, setOpen] = useState<boolean>(false);
    const [text, setText] = useState<string>("");

    function toggleOpen() {
        setOpen(!open);
    }

    function saveComment(store: DataProxy, comment: IComment) {
        const data: { messages: IMessageList } | null = store.readQuery({
            query: messagesQuery,
            variables: queryVars
        });

        const commented = (data!.messages.messages.find((message: IMessage) => message.id === messageId) as IMessage);
        commented.comments.comments.push(comment);
        commented.comments.count++;

        store.writeQuery({
            query: messagesQuery,
            data
        });

        reload();
    }

    return (
        <>
            {open
                ? (
                    <Form>
                        <Form.TextArea
                            placeholder="Comment text"
                            defaultValue={text}
                            onChange={(event, data: TextAreaProps) => setText(data.value ? (data.value as string) : "")}
                        />
                        <Mutation
                            mutation={postComment}
                            variables={{ messageId, text }}
                            update={(store: DataProxy, result: { data?: { postComment: IComment } }) => {
                                saveComment(store, result.data!.postComment);
                            }}
                        >
                            {(postComment: () => void) => 
                                <Button primary style={{ marginBottom: 10 }} size="tiny" compact onClick={() => {
                                    text && postComment();
                                    setText("");
                                    toggleOpen();
                                }}>
                                    Post comment
                                </Button>
                            }
                        </Mutation>
                    </Form>
                ) : ""}
            <Button
                positive={!open}
                negative={open}
                onClick={toggleOpen}
                compact
                size="tiny"
                floated="right"
                title="Reply to message"
            >
                {open ? "Cancel" : "Reply"}
            </Button>
        </>
    );
}

export default CommentForm;
