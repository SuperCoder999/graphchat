import React, { useState } from "react";
import { Form, TextAreaProps, Button } from "semantic-ui-react";
import { Mutation } from "react-apollo";
import postMessage from "../../queries/Mutation/Message/postMessage";
import messages from "../../queries/Query/messages";
import { DataProxy } from "apollo-cache";
import { IMessage, IMessageList } from "../../containers/MessageList/types";

interface MessageFormProps {
    queryVars: any;
    reload: () => void;
}

function MessageForm({ queryVars, reload }: MessageFormProps): JSX.Element {
    const [text, setText] = useState<string>("");

    function saveToStore(store: DataProxy, newMessage: IMessage) {
        const data: { messages: IMessageList } | null = store.readQuery({
            query: messages,
            variables: queryVars
        });

        data!.messages.messages.push(newMessage);
        data!.messages.count++;

        store.writeQuery({
            query: messages,
            data
        });

        reload();
    }

    return (
        <Form className="fluid">
            <Form.TextArea
                defaultValue={text}
                placeholder="Message text"
                rows="6"
                id="textAreaMessage"
                onChange={(event, data: TextAreaProps) => setText(data.value ? (data.value as string) : "")}
            />
            <Mutation
                mutation={postMessage}
                variables={{ text }}
                update={(store: DataProxy, result: { data?: { postMessage: IMessage } }) => {
                    saveToStore(store, result.data!.postMessage);
                }}
            >
                {(postMessage: () => void) =>
                    <Button fluid compact positive onClick={() => {
                        text && postMessage();
                        (document.getElementById("textAreaMessage") as HTMLTextAreaElement).value = "";
                    }}>Add message</Button>
                }
            </Mutation>
        </Form>
    );
}

export default MessageForm;
