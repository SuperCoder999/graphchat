import React from "react";
import Routing from "../../containers/Routing";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { WebSocketLink } from "apollo-link-ws";
import { split, ApolloLink } from "apollo-link";
import { getMainDefinition } from "apollo-utilities"
import { SERVER_LINK } from "../../config";
import { OperationDefinitionNode } from "graphql";

const WSLink = new WebSocketLink({
    uri: `ws://${SERVER_LINK}`,
    options: {
        reconnect: true
    }
});

const httpLink: ApolloLink = createHttpLink({ uri: `http://${SERVER_LINK}` });

const link: ApolloLink = split(
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
        return kind === "OperationDefinition" && operation === "subscription";
    },
    WSLink,
    httpLink
);

const client = new ApolloClient({
    link,
    cache: new InMemoryCache()
});

function Home(): JSX.Element {
    return (
        <ApolloProvider client={client}>
            <Routing />
        </ApolloProvider>
    );
}

export default Home;
