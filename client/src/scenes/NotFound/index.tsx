import React from "react";
import { Grid, Button, Header } from "semantic-ui-react";

function NotFound(): JSX.Element {
    return (
        <Grid columns="1" textAlign="center" verticalAlign="middle" className="fill background-black">
            <Grid.Column style={{ maxWidth: 350 }}>
                <Header as="h1" color="red">Oops! Page not found</Header>
                <Button
                    fluid
                    color="green"
                    inverted
                    onClick={() => window.history.back()}
                >
                    Back
                </Button>
            </Grid.Column>
        </Grid>
    );
}

export default NotFound;
