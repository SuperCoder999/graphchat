import gql from "graphql-tag";

export default gql`
    query messagesQuery($orderBy: MessagesOrderBy, $filterText: String, $skip: Int, $first: Int, $last: Int) {
        messages(orderBy: $orderBy, filterText: $filterText, skip: $skip, first: $first, last: $last) {
            messages {
                id
                text
                likeCount
                dislikeCount
                createdAt
                comments {
                    comments {
                        id
                        text
                        createdAt
                    }
                    count
                }
            }
            count
        }
    }
`;
