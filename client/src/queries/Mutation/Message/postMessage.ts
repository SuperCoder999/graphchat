import gql from "graphql-tag";

export default gql`
    mutation postMessage($text: String!) {
        postMessage(text: $text) {
            id
            text
            likeCount
            dislikeCount
            createdAt
            comments {
                comments {
                    id
                    text
                    createdAt
                }
                count
            }
        }
    }
`;
