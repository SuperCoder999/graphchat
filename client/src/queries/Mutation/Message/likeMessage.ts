import gql from "graphql-tag";

export default gql`
    mutation likeMessage($id: String!) {
        likeMessage(id: $id) {
            id
            text
            likeCount
            dislikeCount
            createdAt
            comments {
                comments {
                    id
                    text
                    createdAt
                }
                count
            }
        }
    }
`;
