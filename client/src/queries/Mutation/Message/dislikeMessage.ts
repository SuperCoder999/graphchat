import gql from "graphql-tag";

export default gql`
    mutation dislikeMessage($id: String!) {
        dislikeMessage(id: $id) {
            id
            text
            likeCount
            dislikeCount
            createdAt
            comments {
                comments {
                    id
                    text
                    createdAt
                }
                count
            }
        }
    }
`;
