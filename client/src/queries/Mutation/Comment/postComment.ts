import gql from "graphql-tag";

export default gql`
    mutation postComment($messageId: String!, $text: String!) {
        postComment(messageId: $messageId, text: $text) {
            id
            text
            createdAt
        }
    }
`;
