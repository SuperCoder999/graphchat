import gql from "graphql-tag";

export default gql`
    subscription {
        newMessage {
            id
            text
            likeCount
            dislikeCount
            createdAt
            comments {
                comments {
                    id
                    text
                    createdAt
                }
                count
            }
        }
    }
`;
