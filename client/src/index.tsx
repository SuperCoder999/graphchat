import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";

import "./styles/reset.scss";
import "semantic-ui-css/semantic.min.css";
import "./styles/common.scss";

import Home from "./scenes/Home";

ReactDOM.render(
    <Home />,
    document.getElementById("root")
);

serviceWorker.register();
