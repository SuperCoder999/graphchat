import React from "react";
import { Router, Route, Switch } from "react-router";
import { createBrowserHistory, BrowserHistory } from "history";
import Chat from "../Chat";
import NotFound from "../../scenes/NotFound";

function Routing(): JSX.Element {
    const history: BrowserHistory = createBrowserHistory();

    return (
        <Router history={history}>
            <Switch>
                <Route path="/" exact component={Chat} />
                <Route path="*" exact component={NotFound} />
            </Switch>
        </Router>
    )
}

export default Routing;
