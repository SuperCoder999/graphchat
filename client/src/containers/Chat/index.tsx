import React from "react";
import MessageList from "../MessageList";
import { Grid } from "semantic-ui-react";

function Chat(): JSX.Element {
    return (
        <Grid columns="1" textAlign="center" className="fill">
            <Grid.Column style={{ maxWidth: 600 }}>
                <MessageList />
            </Grid.Column>
        </Grid>
    );
}

export default Chat;
