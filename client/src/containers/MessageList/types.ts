export interface IMessage {
    id: string;
    text: string;
    createdAt: Date;
    likeCount: number;
    dislikeCount: number;
    comments: ICommentList;
}

export interface IMessageList {
    messages: IMessage[];
    count: number;
}

export interface IComment {
    id: string;
    text: string;
    createdAt: string;
}

export interface ICommentList {
    comments: IComment[];
    count: number;
}

export enum SortType {
    createdAt_ASC = "createdAt_ASC",
    createdAt_DESC = "createdAt_DESC",
    likeCount_ASC = "likeCount_ASC",
    likeCount_DESC = "likeCount_DESC",
    dislikeCount_ASC = "dislikeCount_ASC",
    dislikeCount_DESC = "dislikeCount_DESC"
}

export const SortOptions = [
    { key: 1, value: "createdAt_ASC", text: "Creation date ASC" },
    { key: 2, value: "createdAt_DESC", text: "Creation date DESC" },
    { key: 3, value: "likeCount_ASC", text: "Like count ASC" },
    { key: 4, value: "likeCount_DESC", text: "Like count DESC" },
    { key: 5, value: "dislikeCount_ASC", text: "Dislike count ASC" },
    { key: 6, value: "dislikeCount_DESC", text: "Dislike count DESC" },
];
