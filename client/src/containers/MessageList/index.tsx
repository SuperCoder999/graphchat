import React, { useState } from "react";
import Message from "../../components/Message";
import { Query, QueryResult } from "react-apollo";
import messagesQuery from "../../queries/Query/messages";
import { IMessage, IMessageList, SortType, SortOptions } from "./types";
import MessageForm from "../../components/MessageForm";
import Spinner from "../../components/Spinner";
import { Pagination, Feed, Select, Form, Button } from "semantic-ui-react";
import messagesSubscribe from "../../queries/Subscription/Message/newMessage";
import updateMessageSubscribe from "../../queries/Subscription/Message/updateMessage";
import { MESSAGES_PER_PAGE } from "../../config";

function getPagesCount(itemsCount: number, itemsPerPage: number): number {
    return Math.ceil(itemsCount / itemsPerPage);
}

let subscribed = false;

function MessageList(): JSX.Element {
    const [skip, setSkip] = useState<number>(0);
    const [pageChanged, setPageChanged] = useState<boolean>(false);
    const [sortType, setSortType] = useState<SortType>(SortType.createdAt_ASC);
    const [filterText, setFilterText] = useState<string>("");
    const [fakeState, fakeUpdateState] = useState<boolean>(false);
    const forceUpdate = () => fakeUpdateState(!fakeState);

    function subscribeToMessages(subscribe: any) {
        subscribe({
            document: messagesSubscribe,
            updateQuery: (
                prev: { messages: IMessageList },
                { subscriptionData }: { subscriptionData: { data?: { newMessage: IMessage } } }
            ) => {
                if (!subscriptionData.data) return prev;
                const { newMessage } = subscriptionData.data;

                const exists = Boolean(prev.messages.messages.find((message: IMessage) => {
                    return message.id === newMessage.id;
                }));

                if (exists) return prev;

                return {
                    ...prev,
                    messages: {
                        messages: [...prev.messages.messages, newMessage],
                        count: prev.messages.count + 1,
                        __typename: "MessagesResponse"
                    }
                };
            }
        });

        subscribe({
            document: updateMessageSubscribe,
            updateQuery: (
                prev: { messages: IMessageList },
                { subscriptionData }: { subscriptionData: { data?: { updateMessage: IMessage } } }
            ) => {
                if (!subscriptionData.data) return prev;
                const { updateMessage } = subscriptionData.data;

                const exists = Boolean(prev.messages.messages.find((message: IMessage) => {
                    return message.id === updateMessage.id;
                }));

                if (!exists) return prev;

                const index: number = prev.messages.messages.findIndex(({ id }: { id: string }) => {
                    return id === updateMessage.id;
                });

                const newMessages = [...prev.messages.messages];
                newMessages[index] = updateMessage;

                return {
                    ...prev,
                    messages: {
                        messages: [...newMessages],
                        count: prev.messages.count,
                        __typename: "MessagesResponse"
                    }
                };
            }
        });
    }

    const vars = {
        orderBy: sortType,
        skip,
        ...(!pageChanged ? { last: MESSAGES_PER_PAGE } : {}),
        first: MESSAGES_PER_PAGE,
        ...(filterText ? { filterText } : {})
    };

    return (
        <Feed>
            <Query query={messagesQuery} variables={vars}>
                {
                    ({
                        loading,
                        error,
                        data,
                        refetch,
                        subscribeToMore
                    }: QueryResult<any, Record<string, IMessageList>>): JSX.Element => {
                        if (loading) return <Spinner />;
                        if (error) return <div>Error</div>;
                        const pages = getPagesCount(data.messages.count, MESSAGES_PER_PAGE);

                        if (!subscribed) {
                            subscribeToMessages(subscribeToMore);
                            subscribed = true;
                        }

                        return (
                            <>
                                <MessageForm reload={() => forceUpdate()} queryVars={vars} />

                                <Select
                                    style={{ marginTop: 20 }}
                                    options={SortOptions}
                                    onChange={(event, { value }) => setSortType(value as SortType)}
                                    defaultValue={sortType} />
                                
                                <Form onSubmit={() =>
                                    setFilterText((document.getElementById("filterInput") as HTMLInputElement).value)
                                }>
                                    <Form.Input
                                        placeholder="Enter filter text"
                                        icon="file text"
                                        iconPosition="left"
                                        id="filterInput"
                                        defaultValue={filterText}
                                        style={{ marginTop: 20, marginBottom: 20 }}
                                        autoComplete="off"
                                    />
                                    <Button.Group>
                                        <Button type="submit" positive>Filter</Button>

                                        <Button secondary type="submit" inverted onClick={() =>
                                            (document.getElementById("filterInput") as HTMLInputElement).value = ""}
                                        >Reset filter</Button>
                                    </Button.Group>
                                </Form>

                                {data.messages.messages.map((message: IMessage, index: number): JSX.Element => {
                                    return (
                                        <Message queryVars={vars} message={message} key={index} />
                                    );
                                })}
                                <Pagination
                                    defaultActivePage={pageChanged ? (skip + MESSAGES_PER_PAGE) / MESSAGES_PER_PAGE : pages}
                                    totalPages={pages}
                                    boundaryRange={1}
                                    siblingRange={1}
                                    onPageChange={(e, { activePage }) => {
                                        setSkip(MESSAGES_PER_PAGE * Number(activePage) - MESSAGES_PER_PAGE);
                                        setPageChanged(true);
                                        refetch(data);
                                    }}
                                />
                            </>
                        );
                    }
                }
            </Query>
        </Feed>
    );
}

export default MessageList;
