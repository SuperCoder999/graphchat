import React from "react";
import Comment from "../../components/Comment";
import { IComment } from "../MessageList/types";
import { Comment as CommentUI } from "semantic-ui-react";
import CommentForm from "../../components/CommentForm";

interface CommentListProps {
    comments: IComment[];
    messageId: string;
    queryVars: any;
}

class CommentList extends React.Component<CommentListProps> {
    render() {
        const { comments, messageId, queryVars }: CommentListProps = this.props;

        return (
            <CommentUI.Group className="text-left">
                {comments.map((comment: IComment, index: number): JSX.Element => {
                    return <Comment comment={comment} key={index} />;
                })}
                <CommentForm messageId={messageId} reload={() => this.forceUpdate()} queryVars={queryVars} />
            </CommentUI.Group>
        );
    }
}

export default CommentList;
